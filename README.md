Candl 0.1
Written by Makvas

Using
=====

The Candl (Conlangs and Languages) tool is for those wanting to automate complex
language morphological and phonetic rules. It uses regular expressions
extensively to define these rules.

This tool is not very user-friendly at the moment, but I invite others to make
changes as needed, and encourage them to publish useful modifications.

Syntax and semantics are still left to the language crafter to decide, but much
else is automated. This tool can therefore help prevent typos and speed up
translation efforts.

To use this program, you have to prepare several files. These are explained in
the following sections.

Dictionary
----------

In a file named something like 'mylang.dict', you should provide a list of basic
words (in whatever form you like). The basic syntax is

    word: identifier

The identifier is typically some unique english word or phrase you associate
with the word, for use in glossing.

You can also group words into categories. The usefulness of this is explained
later with rewrite rules. Here is the syntax:

    word (class1, class2): identifier

Entries are separated by new lines.

An example:

    manzana (noun, fem): apple

Grammar Rewrite Rules
---------------------

These are named regex substitutions. When a rule is applied to an identifier, it
causes the regex substitution to be applied to the corresponding word.
There are several syntaxes allowed for grammar rewrite rules, making both
prefix and postfix notation available as needed:

    var=content
    -rule regex > substitution # comments
    =rule regex > substitution # comments
    ~rule regex > substitution # comments
    <rule> regex > substitution # comments
    rule- regex > substitution # comments
    rule= regex > substitution # comments
    rule~ regex > substitution # comments
    <rule> regex > substitution # comments
    class-rule regex > substitution # comments
    class=rule regex > substitution # comments
    class~rule regex > substitution # comments
    class<rule> regex > substitution # comments
    rule-class regex > substitution # comments
    rule=class regex > substitution # comments
    rule~class regex > substitution # comments
    <rule>class regex > substitution # comments

And an example:

    C = [^aeiou]
    V = [aeiou]
    -PLR (C)$ > \1es
         (V)$ > \1s

A different substitution command is applied, in order, for each line.
So rule "-PLR" adds an "s" onto the end of any word ending with a vowel,
and "es" otherwise. These commands can get much more complex.

Identifiers often have a period when representing fusional morphemes:

    -PAST.PERF er$ > ía

Variables are basically meta-rewrite rules; when defining consonants as C for
example, the subsitution C > [^aeiou] is run on the subsequent lines in the
file.

As another example, if a past-tense prefix is na- before consonants and u/i,
but n- before other vowels, you could write it like

    -PAST ^([^aeo]) > na\1
          ^([aeo]) > n\1

You can get more complicated, however. You can specify that a grammar rewrite
rule applies differently to words in different categories. Here is the syntax
for defining the plural rule as -so for class1, and as -na for class2:

    # if word is in class1
    class1-PLR $ > so
               
    # or, in class 2
    class2-PLR $ > na

Phonology Rewrite Rules
-----------------------

Again, these are regex substitutions. Use them to rewrite words or parts of
words into a phonetic system. Syntax:

    var=content
    regex substitution # comment

Example:

    V=[aeiou]
    th θ
    bh v

Phonology rewrite rules apply always, so they don't have identifiers.

Input Files
-----------

Input files are written with the following syntax

    word1-suffix1-suffix2 word2 ...
    "comments"

where suffix1 and suffix2 are names of rewrite rules, and word1 and word2 are
identifiers for words or stems. The hyphen can be replaced with several symbols:
'=rule', ':rule', '~rule', or '<rule>', as appropriate.

These rules are meant to mimic Leipzig glossing rules.

Example
-------

Given this dictionary file ('example.dict'):

    skom (v): run
    narum (n): store
    atl (pn): 3SG

And this grammar rules file ('example.grammar'):

    -PAST $ > yo
    to= ^ > au
    3SG-SUBJ $ > u

And this phonology rewrite file ('example.phonology'):
    
    # Rewrite some letters to appropriate IPA
    y > j
    l > ɬ
    # Some useful phoneme groupings (V for vowels, C for consonants)
    V=[aeiou]
    C=(?:[ptknmsjɬ]|tɬ)
    # break syllables apart:
    (V)(CV) > \1\.\2
    (VC)(CV) > \1\.\2
    (V)(VV) > \1\.\2
    # Primary stress rules:
    (\.|^)([^\.]+\.[^\.]+$) > ˈ\2  # This puts the stress on the next-to-last syllable

An input file ('example.in') might look like this:

    run-PAST store=to 3SG-SUBJ
    "He ran to the store"

Running this command,

    $ candl dictionary.dict grammar.rule phonology.rule example.in example.out

The corresponding output file ('example.out') would look like:
    
    (1) ˈskom.jo  auˈna.rum   aˈtɬu
         skomyo   aunarum     atlu
         run-PAST to=store    3SG-SUBJ
        "He ran to the store"

Support for LaTeX output is planned.
