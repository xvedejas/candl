#!/usr/bin/python3

import re, sys

class LexicalEntry():
    def __init__(self, identifier, word, categories=None):
        self.identifier = identifier
        self.word = word
        self.categories = categories or []
    
    def __repr__(self):
        return repr((self.identifier, self.word, self.categories))

class Lexicon():
    """A "dictionary" of words, a mapping from identifiers to words in the
       language"""
    entry_pattern = r'(\S+)\s*(\(.*?\))?:\s+(.*)'
    
    @classmethod
    def parse(cls, code: list):
        """Parse in a dictionary from code. (Alternate constructor)"""
        entries = {}
        categories = {}
        for line in code:
            match = re.match(Lexicon.entry_pattern, line)
            word, these_categories, identifier = match.groups()
            
            # Find the category names by further parsing these_categories
            
            if these_categories:
                these_categories = these_categories[1:-1].split(',')
            else:
                these_categories = []
            
            # Each identifier is itself a category.
            these_categories.append(identifier)
            
            for this_category in these_categories:
                this_category = this_category.strip()
                if this_category not in categories:
                    categories[this_category] = []
            
            categories[identifier].append(identifier)
            
            entries[identifier] = LexicalEntry(identifier, word, these_categories)
        
        return Lexicon(entries, categories)
    
    def __init__(self, entries: dict, categories: dict):
        """The "definitions" are a mapping from identifier to word,
           and the "categories" are a mapping from a categoryname to a list of
           identifiers in that category. """
        self.entries = entries
        self.categories = categories
    
    def __iter__(self):
        return iter(self.entries)
    
    def __getitem__(self, identifier):
        return self.entries[identifier]

class GrammarRule():
    """A rule starts with a 'full name' of one of these sorts:
    
    Postfix:
        -rule
        ~rule
        =rule
        :rule
        <rule                      (to be written identifier<rule> in input)
    Prefix:
        rule-
        rule~
        rule=
        rule:
        rule>                      (to be written <rule>identifier in input)
    Postfix, specific class:
        classname-rule
        classname~rule
        classname=rule
        classname:rule
        classname<rule             (to be written identifier<rule> in input)
    Prefix, specific class:
        rule-classname
        rule~classname
        rule=classname
        rule:classname
        rule>classname             (to be written <rule>identifier in input)
        
    We need to keep track of the rule name, class name, prefix v postfix,
    and delimiter to know how this rule can be applied.
    
    If, in the input code, we find a rule which isn't defined, we will try to
    treat it as a word.
    """
    rule_delimiters = '([\-~=:<>])'
    
    @classmethod
    def parse(self, fullname, pattern, replacement, all_categories):
        first, delimiter, last = re.split(GrammarRule.rule_delimiters, fullname)
        category = None
        rulename = None
        
        if first == '': # Postfix (no specific category)
            rulename = delimiter + last + ('>' if delimiter == '<' else '')
        elif first in all_categories: # Postfix, specific category/identifier
            rulename = delimiter + last + ('>' if delimiter == '<' else '')
            category = first
        elif last == '': # Prefix (no specific category)
            rulename = ('<' if delimiter == '>' else '') + first + delimiter
        elif last in all_categories: # Prefix, specific category/identifier
            rulename = ('<' if delimiter == '>' else '') + first + delimiter
            category = last
        if not rulename:
            raise Exception("Invalid rule. Does your category exist?")
        
        return GrammarRule(rulename, pattern, replacement, category)
        
    def __init__(self, name, pattern, replacement, category=None):
        self.category = category
        self.name = name
        self.patterns = [pattern]
        self.replacements = [replacement]
    
    def add_substitution(self, pattern, replacement):
        self.patterns.append(pattern)
        self.replacements.append(replacement)
    
    def apply(self, line):
        """This applies the rule to all non-overlapping instances in the
           given line of words."""
        for pattern, replacement in zip(self.patterns, self.replacements):
            line = re.sub(pattern, replacement, line)
        return line

class Grammar():
    rule_continuation_pattern = r"\s*?(.+?)\s+?[-=~]?>\s+(.+)"
    rule_declaration_pattern = (r"^([^-=~<> ]*?[-=~][^-=~<> ]*?|[^<]*?<[^>]+?>|<[^>]+?>[^ ]*?)"
                                r"\s+?(.+?)\s+?[-=~]?>\s+(.+)")
    variable_declaration_pattern = r"([^=]+)=([^#]+)"
    
    @classmethod
    def parse(cls, code: list, categories: list):
        rules = {}
        variables = []
        for line in code:
            # Apply all variables to this line
            for variable, value in variables:
                line = re.sub(variable, value, line)
            
            # Now, try matching as a rule declaraction
            match = re.match(Grammar.rule_declaration_pattern, line)
            if match:
                rule, regex, substitution = match.groups()
                grammar_rule = GrammarRule.parse(rule, regex, substitution, categories)
                rules[grammar_rule.name] = grammar_rule
                continue
            # If that didn't work, try matching as a variable declaraction
            match = re.match(Grammar.variable_declaration_pattern, line)
            if match:
                variable, value = match.groups()
                variables.append((variable, value))
                continue
            match = re.match(Grammar.rule_continuation_pattern, line)
            if not match:
                raise Exception("Invalid grammar rule syntax")
            if not grammar_rule:
                raise Exception("Found unnamed grammar rule")
            regex, substitution = match.groups()
            grammar_rule.add_substitution(regex, substitution)
            
        return Grammar(rules)
    
    def __init__(self, rules: dict):
        """The "rules" are a mapping from rulename to (regex, substitution)
           tuples."""
        self.rules = rules
    
    def __iter__(self):
        return iter(self.rules)
    
    def __getitem__(self, rulename):
        return self.rules[rulename]

class PhonologicalRule():
    def __init__(self, pattern, replacement):
        self.pattern = pattern
        self.replacement = replacement
    
    def apply(self, line):
        """This applies the rule to all non-overlapping instances in the
           given line of words."""
        return re.sub(self.pattern, self.replacement, line)

class Phonology():
    rule_declaration_pattern = r"\s*(.+?)\s+?[-=~]?>\s*(.+)"
    variable_declaration_pattern = r"\s*([^=]+)=\s*(.+)"
    
    @classmethod
    def parse(cls, code: list):
        rules = []
        variables = []
        for line in code:
            # Apply all variables to this line
            for variable, value in variables:
                line = re.sub(variable, value, line)
            
            # Now, try matching as a rule declaraction
            match = re.match(Phonology.rule_declaration_pattern, line)
            if match:
                regex, substitution = match.groups()
                rule = PhonologicalRule(regex, substitution)
                rules.append(rule)
                continue
            # If that didn't work, try matching as a variable declaraction
            match = re.match(Phonology.variable_declaration_pattern, line)
            if match:
                variable, value = match.groups()
                variables.append((variable, value))
                continue
            
        return Phonology(rules)
        
    def __init__(self, rules: list):
        self.rules = rules
    
    def __iter__(self):
        return iter(self.rules)
    
    def __getitem__(self, rulename):
        return self.rules[rulename]

class Language():
    def __init__(self, lexicon: Lexicon,
                       grammar_rules: Grammar,
                       phonetic_rules: Phonology):
        self.lexicon = lexicon
        self.grammar_rules = grammar_rules
        self.phonetic_rules = phonetic_rules
        
    def translate(self, gloss: str) -> str:
        """A "gloss" is made up of "commands". Each command has one identifier
           and zero or more rules."""
        grammar_out = []
        phonetic_out = []
        
        for command in gloss.split():
            
            components = re.split(GrammarRule.rule_delimiters, command)
            ncomponents = len(components)
            
            # Iterate through components
            identifier = None
            rules = []
            i = 0
            while i < ncomponents:
                if components[i] in self.lexicon:
                    identifier = components[i]
                    i += 1
                else:
                    rules.append(components[i] + components[i+1])
                    i += 2
            
            assert identifier
            
            entry = self.lexicon[identifier]
            word = entry.word
            categories = entry.categories
            
            # Apply Grammar Rules
            for rulename in rules:
                grammar_rule = self.grammar_rules[rulename]
                
                # Check that the rule applies to this category, if restricted
                if not grammar_rule.category or grammar_rule.category in categories:
                    word = grammar_rule.apply(word)
            
            grammar_out.append(word)
            
            # Apply Phonetic Rules
            for rule in self.phonetic_rules:
                word = rule.apply(word)
            
            phonetic_out.append(word)
            
        return ' '.join(phonetic_out) + '\n' + ' '.join(grammar_out)

def parse(dictionary_code: list, grammar_code: list, phonology_code: list):
    """Given code as a list of lines, parses into a language"""
    lexicon = Lexicon.parse(dictionary_code)
    grammar = Grammar.parse(grammar_code, lexicon.categories.keys())
    phonology = Phonology.parse(phonology_code)
    return Language(lexicon, grammar, phonology)

def read_entire_file(path, remove_comments=True):
    """Reads a file into a list of lines"""
    contents = []
    with open(path) as f:
        for line in f:
            if remove_comments:
                line = re.sub('#.*$', '', line)
            contents.append(line.strip())
    return contents

def main(dictionary_path, grammar_path, phonology_path, input_path):
    """Given file paths, let's generate the gloss"""
    dictionary_code = read_entire_file(dictionary_path)
    grammar_code = read_entire_file(grammar_path)
    phonology_code = read_entire_file(phonology_path)
    input_code = read_entire_file(input_path)
    language = parse(dictionary_code, grammar_code, phonology_code)
    for line in input_code:
        # Ignore lines in double quotes
        if not re.match('\s*".*?"\s*', line):
            print(language.translate(line))
        print(line)

if __name__ == '__main__':
    # Uncomment if you want to specify files to read from the command line
    #main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
    main('example.dict', 'example.grammar', 'example.phonology', 'example.in')
